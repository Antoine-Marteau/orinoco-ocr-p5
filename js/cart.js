function addToCart() {

    let itemId = document.querySelector('[data-id]').dataset.id;
    let itemLense = document.getElementById('options').value;
    let itemQuantity = parseInt(document.getElementById('quantity').value);

    //Si l'ID du produit est déjà renseigné
    if (cart["products"][itemId]) {

        //Si la lentille est déjà renseignée
        if (cart["products"][itemId][itemLense]) {

            //Addition de la valeur actuelle à la valeur existante
            let cartItemQuantity = parseInt(cart["products"][itemId][itemLense]["quantity"]);
            cart["products"][itemId][itemLense]["quantity"] = itemQuantity + cartItemQuantity;

        } else {

            //Ajout de la nouvelle lentille
            cart["products"][itemId][itemLense] = { "quantity": itemQuantity };
        }

    } else {

        //Ajout de l'ID du nouveau produit
        let lenseQuantity = { 'quantity': itemQuantity };

        let lenseName = {};
        lenseName[itemLense] = lenseQuantity;

        cart["products"][itemId] = lenseName;
    }

    //Mise à jour de la quantité globale de produits
    cart.itemsQuantity = parseInt(cart.itemsQuantity) + itemQuantity;

    displayDotCart(cart.itemsQuantity);
    setLocalStorage(cart);
}

function removeFromCart(e) {

    let target = e.currentTarget;

    //Évènement sur l'input de quantité
    if (target.dataset.actual) {

        let oldValue = parseInt(target.dataset.actual);
        let newValue = parseInt(target.value);

        //MAJ des valeurs de cart
        cart["products"][target.dataset.id][target.dataset.lense]["quantity"] = newValue;
        cart["itemsQuantity"] = (cart["itemsQuantity"] - oldValue) + newValue;

        //Évènement sur l'input checkbox
    } else {

        let deleteQuantity = parseInt(target.nextElementSibling.getElementsByClassName("cart-quantity")[0].dataset.actual);

        //MAJ de cart
        cart["itemsQuantity"] = cart["itemsQuantity"] - deleteQuantity;
        delete cart["products"][target.dataset.id][target.dataset.lense];

        //Suppression de l'ID si toute les lentilles du modèle ont été supprimées
        if (!Object.keys(cart["products"][target.dataset.id]).length) {
            delete cart["products"][target.dataset.id];
        }
    }

    //Mise à jour de l'affichage
    displayDotCart(cart["itemsQuantity"]);
    totalPrice();
    setLocalStorage(cart);
}