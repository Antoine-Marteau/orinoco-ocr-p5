function resetQuantityInput() {

    document.getElementById('quantity').value = 1;
}

/* --- ÉVÉNEMENTS --- */
function cartButtonEvent(e) {

    addToCart(e);
    resetQuantityInput();
}

/* --- INITIALISATION DES ÉVÉNEMENTS --- */
function setCartEvent() {

    //Ajout de l'évènement sur le bouton d'ajout
    let dataId = document.querySelectorAll('[data-id]');
    for (let i = 0; i < dataId.length; i++) {
        dataId[i].addEventListener("click", function (e) {
            cartButtonEvent(e);
        });
    }
}

/* --- RÉCUPÉRATION DES DONNÉES DEPUIS L'API --- */
function getProductData() {

    let actualUrl = window.location.href;
    let actualUrlObj = new URL(actualUrl);
    let urlId = actualUrlObj.search.split('?id=')[1];

    let productData = fetch("http://localhost:3000/api/cameras/" + urlId)
        .then(function (result) {
            return result.json();
        })
        .then(function (data) {
            return data;
        });

    return productData;
}

async function getProductTemplate() {

    let productLayout = '';
    let lensesLayout = '';

    //Récupération des lentilles du produit
    let cameraInfos = await getProductData();

    //Préparation de l'affichage des lentilles
    cameraInfos.lenses.forEach(option => {
        lensesLayout += `<option class="option" value="${option}">${option}</option>`;
    });

    productLayout += `
        <figure class="product-img-container">
            <img src=${cameraInfos.imageUrl} alt="" class="product-img">
        </figure>
        <div class="infos">
            <h1 class="h1 h1-product">${cameraInfos.name}</h1>
            <p class="description">${cameraInfos.description}</p>
            <label for="options" class="option-name">Choix de la lentille :</label>
            <select class="options" id="options">`
        + lensesLayout +
        `</select>
            <input type="number" id="quantity" name="quantity" min="1" value="1">
            <span class="buying">
                <p class="price">${cameraInfos.price} $</p>
                <button class="button-item button-item-add" data-id=${cameraInfos._id}>Ajouter au panier</button>
            </span>
        </div>`;

    return productLayout;
}

async function displayProduct() {

    let template = await getProductTemplate();
    document.getElementsByClassName('product-infos')[0].innerHTML = template;
}

/* --- INITIALISATION DE LA PAGE --- */
function initProductPage() {

    var cart = null;
    getLocalStorage();
    displayProduct()
        .then(function () {
            setCartEvent();
        })
        .catch(function () {
            document.getElementsByClassName('product-infos')[0].innerHTML = "<span class='error_msg'>Une erreur nous empêche d'afficher la page produit</span>";
        })
}

document.addEventListener("DOMContentLoaded", initProductPage);