function totalPrice() {

    let prices = document.querySelectorAll(".cart-item-checkbox");
    let finalPrice = 0;

    //Calcul du prix total en récupérant la quantité et le prix unitaire sur chaque élément
    prices.forEach(price => {

        if (price.checked) {

            let quantity = parseInt(price.nextElementSibling.querySelector(".cart-quantity").value);
            price = parseInt(price.nextElementSibling.querySelector("[data-price]").dataset.price);

            finalPrice += price * quantity;
        }
    })
    document.getElementsByClassName("sticker-number")[0].innerHTML = finalPrice;
}

function displayForm() {

    document.getElementsByClassName("main-cart")[0].innerHTML += `
        
        <form class="cmd-form" action="confirm.html" method="POST">
            <span class="cmd-title">Informations de la commande</span>
            <span class="cmd-info">
                <label for="firstName" class="cmd-label">First name</label>
                <input class="cmd-input" type="text" id="firstName" name="firstName"
                    pattern="[A-zÀ-ú.,\\- ]*" 
                    required>
            </span>
            <span class="cmd-info">
                <label for="lastName" class="cmd-label">Last name</label>
                <input class="cmd-input" type="text" id="lastName" name="lastName"
                    pattern="[A-zÀ-ú.,\\- ]*" 
                    required>
            </span>
            <span class="cmd-info">
                <label for="address" class="cmd-label">Adresse</label>
                <input class="cmd-input" type="text" id="address" name="address"
                    pattern="[A-zÀ-ú0-9.,\\- ]*" 
                    required>
            </span>
            <span class="cmd-info">
                <label for="city" class="cmd-label">City</label>
                <input class="cmd-input" type="text" id="city" name="city"
                    pattern="[A-zÀ-ú0-9.,\\- ]*" 
                    required>
            </span>
            <span class="cmd-info">
                <label for="email" class="cmd-label">E-mail</label>
                <input class="cmd-input" type="text" id="email" name="email" placeholder="email@valid.com"
                    pattern="^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$"
                    required>
            </span>
            <!-- <input type="hidden" name="products" value="" class="formProducts"> -->
            <button type="submit" class="cmd-submit">Passer commande</button>
        </form>

    `;
}

/* --- ÉVÉNEMENTS --- */
function addFromCartContent(e) {

    let target = e.currentTarget;

    if (target.dataset.actual) {

        //Si c'est un input quantité
        let oldValue = parseInt(target.dataset.actual);
        let newValue = parseInt(target.value);

        cart.itemsQuantity = cart.itemsQuantity + (newValue - oldValue);

        cart["products"][target.dataset.id][target.dataset.lense]["quantity"] = newValue;

    } else {

        //Si c'est un input checkbox
        let quantityInput = target.nextElementSibling.getElementsByClassName("cart-quantity")[0];
        let quantity = parseInt(quantityInput.value);

        let productId = quantityInput.dataset.id;
        let productLense = quantityInput.dataset.lense;

        cart["itemsQuantity"] = cart["itemsQuantity"] + quantity;

        //Si l'ID est déjà enregistré
        if (cart["products"][productId]) {

            cart["products"][productId][productLense] = { "quantity": quantity };

            //Si l'ID n'est pas enregistré
        } else {

            let lenseQuantity = { "quantity": quantity };
            let lenseId = {};
            lenseId[productLense] = lenseQuantity;
            cart["products"][productId] = lenseId;
        }
    }

    //Mise à jour de l'affichage
    displayDotCart(cart["itemsQuantity"]);
    totalPrice();
    setLocalStorage(cart);
}

function quantityInputEvent(e) {

    let currentValue = parseInt(e.currentTarget.value);
    let lastValue = parseInt(e.currentTarget.dataset.actual);

    //Détermine quelle fonction appeler selon les valeurs trouvées
    currentValue > lastValue ? addFromCartContent(e) : removeFromCart(e);

    //MAJ de la balise data
    e.currentTarget.dataset.actual = currentValue;
}

function checkInputEvent(e) {

    let checked = e.currentTarget.checked;
    let quantity = e.currentTarget.nextElementSibling.getElementsByClassName("cart-quantity")[0];

    //Détermine quelle fonction appliquer selon l'état du check
    if (checked) {

        addFromCartContent(e);
        quantity.disabled = false;
    } else {

        removeFromCart(e);
        quantity.disabled = true;
    }
}

function beforeSubmit() {

    var cmdButton = document.getElementsByClassName("cmd-submit")[0];
    cmdButton.onclick = function () {

        //Enregistrement des données de contact avant de passer sur la page suivante
        cart.formInfos = {
            "firstName": document.getElementById("firstName").value,
            "lastName": document.getElementById("lastName").value,
            "address": document.getElementById("address").value,
            "city": document.getElementById("city").value,
            "email": document.getElementById("email").value,
        };

        setLocalStorage(cart);
    };
}

/* --- INITIALISATION DES ÉVÉNEMENTS --- */
function setCheckEvent() {

    var checkInput = document.getElementsByClassName('cart-item-checkbox');

    for (checkItem of checkInput) {

        checkItem.addEventListener('click', function (e) {

            checkInputEvent(e);
        })
    }
}

function setQuantityEvent() {

    var quantityInput = document.getElementsByClassName('cart-quantity');
    for (quantityItem of quantityInput) {
        quantityItem.addEventListener('change', function (e) {
            quantityInputEvent(e);
        })
    }
}

/* --- RÉCUPÉRATION DES DONNÉES DEPUIS L'API --- */
async function displayCart() {

    var allProductsCart = [];

    for (product in cart.products) {
        allProductsCart.push(fetch("http://localhost:3000/api/cameras/" + product));
    }

    //récupération des informations pour chaque produit du panier
    let cartProductData = Promise.all(allProductsCart)
        .then((values) => {

            let allProductsCartJson = [];

            values.forEach(element => {
                allProductsCartJson.push(element.json());
            })

            let processedProducData = Promise.all(allProductsCartJson)
                .then((data) => {

                    let dataTemplateCart = [];

                    data.forEach(element => {

                        for (lense in cart["products"][element._id]) {
                            let lenseQuantity = cart["products"][element._id][lense]["quantity"];
                            dataTemplateCart.push({ element, lense, lenseQuantity });
                        }
                    })

                    return dataTemplateCart;
                })

            return processedProducData;
        })

    return await cartProductData;
}

async function templateCart() {

    let partialTemplateCart = "";
    let data = await displayCart();

    data.forEach(dataItem => {

        partialTemplateCart += `
            <li class="cart-item-list">
                <input class="cart-item-checkbox" type="checkbox" name="${dataItem.element._id + dataItem.lense}" id="${dataItem.element._id + dataItem.lense}" value="${dataItem.element._id}" data-id=${dataItem.element._id} data-lense="${dataItem.lense}" checked>
                <div class="cart-item-container">
                    <figure class="cart-img-container">
                        <img src="${dataItem.element.imageUrl}" alt="" class="cart-img">
                    </figure>
                    <span class="cart-infos">
                        <span class="cart-product-name">${dataItem.element.name}</span>
                        <span class="cart-option">${dataItem.lense}</span>
                        <span data-price=${dataItem.element.price} class="cart-price">${dataItem.element.price} $</span>
                    </span>
                    <span class="cart-inputs">
                        <input type="number" class="cart-quantity" min="1" value="${dataItem.lenseQuantity}" data-actual=${dataItem.lenseQuantity} data-id=${dataItem.element._id} data-lense="${dataItem.lense}">
                        <label for="${dataItem.element._id + dataItem.lense}" class="cart-label icon-check"></label>
                    </span>
                </div>
            </li>
        `;

    })
    let sticker = `
        <div class="sticker">
            <span class="sticker-total">total :</span>
            <span class="sticker-price"><span class="sticker-number"></span> $</span>
        </div>`;

    let fullTemplateCart = "<div class='cart-container'><ul class='cart'>" + partialTemplateCart + "</ul>" + sticker + "</div>";

    return fullTemplateCart;
}

async function displayTemplateCart() {

    let template = await templateCart();
    document.getElementsByClassName('main-cart')[0].innerHTML += template;
}

/* --- INITIALISATION DE LA PAGE --- */
function initCartcontentPage() {

    var cart = getLocalStorage();

    //Affiche la page seulement si le panier contient des items
    if (cart && cart.itemsQuantity > 0) {


        displayTemplateCart()
            .then(
                function () {
                    displayForm();
                    setQuantityEvent();
                    setCheckEvent();
                    beforeSubmit();
                    totalPrice();
                })
            .catch(function () {
                document.getElementsByClassName('main-cart')[0].innerHTML += '<span class="error_msg">Une erreur nous empêche d\'afficher le panier</span>';
            });
    } else {
        document.getElementsByClassName('main-cart')[0].innerHTML += '<span class="error_msg">Aucun article dans le panier</span>';
    }
}

document.addEventListener("DOMContentLoaded", initCartcontentPage)