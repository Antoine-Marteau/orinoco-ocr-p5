async function getIndexData() {

    let indexData = fetch("http://localhost:3000/api/cameras/")
        .then(function (result) {
            return result.json();
        })
        .then(function (data) {
            return data;
        });

    return indexData;
}

async function getTemplateProducts() {

    let cameraInfos = await getIndexData();
    let templateProducts = '';

    cameraInfos.forEach(element => {
        templateProducts += `<div class="index-item">
                <figure class="index-img-container">
                    <img src=${element.imageUrl} alt="" class="index-img">
                </figure>
                <span class="top-item">
                    <p class="name">${element.name}</p>
                    <p class="price">${element.price} $</p>
                </span>
                <p class="description">${element.description}</p>
                <a href="pages/product.html?id=${element._id}" class="button-item">Détails</a>
            </div>`;
    });

    return templateProducts;
}

async function displayIndexProduct() {

    let template = await getTemplateProducts();
    document.getElementsByClassName('index-list')[0].innerHTML = template;
}

/* --- INITIALISATION DE LA PAGE --- */
function initIndexPage() {

    var cart = null;
    getLocalStorage();
    displayIndexProduct()
        .catch(function () {
            document.getElementsByClassName('index-list')[0].innerHTML = "<span class='error_msg'>Une erreur nous empêche d'afficher la grille des produits</span>";
        });
}

document.addEventListener("DOMContentLoaded", initIndexPage)