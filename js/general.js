function displayDotCart(total) {

    let cartLogo = document.getElementsByClassName('cart-logo')[0];

    //Affiche / supprime le point selon les items dans le caddie
    if (total > 0) {
        cartLogo.setAttribute("data-cart", total);
        cartLogo.classList.add('dotted');
    } else {
        cartLogo.classList.remove('dotted');
    }
}

function getLocalStorage() {

    //Attribut une valeur vide par défault à cart
    if (localStorage.length === 0) {
        localStorage.cart = '{ "products" : {}, "itemsQuantity" : 0 }';
    }

    cart = JSON.parse(localStorage.cart);

    let bodyClass = document.getElementsByTagName('body')[0].className;

    //Affiche le nombre d'items achetés seulement si il n'est pas sur la page de confirmation
    if (bodyClass !== 'page-confirm') {
        displayDotCart(cart.itemsQuantity)
    }

    return cart;
}

function setLocalStorage(cart) {

    localStorage.cart = JSON.stringify(cart);
}