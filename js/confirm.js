/* --- ERREURS --- */

function confirmErrorAPI(mainMsg) {

    //document.getElementsByClassName('cmd-form')[0].innerHTML += "<span class='error_msg'>Une erreur technique nous empêche d'enregistrer votre commande</span>";
    document.getElementsByClassName(mainMsg)[0].innerHTML += "<span class='error_msg'>Une erreur technique nous empêche d'enregistrer votre commande</span>";
    document.getElementsByClassName('h1')[0].innerHTML = "Quelque chose s'est mal passé...";
    displayDotCart(cart.itemsQuantity);
}

function confirmErrorNoItem() {

    document.getElementsByClassName('main-cart')[0].innerHTML += "<span class='error_msg'>Vous n'avez aucun article dans votre panier</span>";
    document.getElementsByClassName('h1')[0].innerHTML = "Quelque chose s'est mal passé...";
    displayDotCart(cart.itemsQuantity);
}

/* --- PASSAGE DE LA COMMANDE --- */
function totalPriceOrder() {

    let prices = document.querySelectorAll(".cart-item-container");
    let finalPrice = 0;

    prices.forEach(price => {

        let quantity = price.dataset.quantity;
        price = price.dataset.price;

        finalPrice += parseInt(price * quantity);
    })

    return finalPrice;
}

async function passOrder() {

    let products = [];

    for (productItem in cart.products) {
        products.push(productItem);
    }

    //Informations de la commande
    let info = {
        "contact": cart.formInfos,
        "products": products
    };

    //Options de la fonction fetch
    let options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(info)
    };

    //Passage de la requète pour la commande
    let dataOrder = fetch("http://localhost:3000/api/cameras/order", options)
        .then(function (res) {
            let responseJSON = res.json();
            return responseJSON;
        })
        .then(function (da) {
            return da;
        });

    return await dataOrder;
}

async function templateResult() {

    let dataOrder = await passOrder();

    let template = `
        <span class="cmd-title">Informations de la commande</span>
        <span class="cmd-info">
            <span class="cmd-label">First name : </span>
            <span class="cmd-final">${dataOrder.contact.firstName}</span>
        </span>
        <span class="cmd-info">
            <span class="cmd-label">Last name : </span>
            <span class="cmd-final">${dataOrder.contact.lastName}</span>
        </span>
        <span class="cmd-info">
            <span class="cmd-label">Adresse : </span>
            <span class="cmd-final">${dataOrder.contact.address}</span>
        </span>
        <span class="cmd-info">
            <span class="cmd-label">City : </span>
            <span class="cmd-final">${dataOrder.contact.city}</span>
        </span>
        <span class="cmd-info">
            <span class="cmd-label">E-mail : </span>
            <span class="cmd-final">${dataOrder.contact.email}</span>
        </span>
        <span class="cmd-info">
            <span class="cmd-label">Prix payé : </span>
            <span class="cmd-final">${totalPriceOrder()} $</span>
        </span>
        <span class="cmd-info">
            <span class="cmd-label">ID de la commande : </span>
            <span class="cmd-final">${dataOrder.orderId}</span>
        </span>
        <a href="../index.html" class="cmd-submit">Retour à l'accueil</a>
    `;

    return template;
}

async function displayOrderInfos() {

    let orderTemplate = await templateResult();

    //MAJ de l'affichage quand la commande est passée et affichée
    document.getElementsByClassName('h1')[0].innerHTML = 'Merci pour votre achat !';
    document.getElementsByClassName('cart-container')[0].innerHTML += '<div class="sticker"><span class="sticker-vendu">Vendu !</span></div>';
    document.getElementsByClassName('cmd-form')[0].innerHTML = orderTemplate;
}

/* --- RÉCUPÉRATION DES DONNÉES DEPUIS L'API --- */
async function getCartData() {

    var allCarts = [];

    //Récupération des données du caddie
    for (product in cart.products) {
        allCarts.push(fetch("http://localhost:3000/api/cameras/" + product));
    }

    let cartData = Promise.all(allCarts)
        .then((values) => {

            let allCartsJson = [];

            values.forEach(element => {
                allCartsJson.push(element.json());
            })

            let cartProcessedData = Promise.all(allCartsJson)
                .then((data) => {

                    let objectCartData = [];
                    data.forEach(element => {

                        for (lense in cart["products"][element._id]) {
                            let lenseQuantity = cart["products"][element._id][lense]["quantity"];
                            objectCartData.push({ element, lense, lenseQuantity });
                        }
                    })
                    return objectCartData;
                })

            return cartProcessedData;
        })

    return await cartData;
}

async function templateCart() {

    let itemCartTemplate = "";
    let data = await getCartData();

    data.forEach(dataItem => {

        itemCartTemplate += `
            <li class="cart-item-list">
                <!-- <input class="cart-item-checkbox" type="checkbox" name="${dataItem.element._id + dataItem.lense}" id="${dataItem.element._id + dataItem.lense}" value="${dataItem.element._id}" checked> -->
                <div class="cart-item-container confirmed" data-price=${dataItem.element.price} data-quantity=${dataItem.lenseQuantity} >
                    <figure class="cart-img-container">
                        <img src="${dataItem.element.imageUrl}" alt="" class="cart-img">
                    </figure>
                    <span class="cart-infos">
                        <span class="cart-product-name">${dataItem.element.name}</span>
                        <span class="cart-option">${dataItem.lense}</span>
                        <span data-price=${dataItem.element.price} class="cart-price">${dataItem.element.price} $</span>
                    </span>
                    <span class="cart-inputs">
                        <span class="cart-quantity">X ${dataItem.lenseQuantity}</span>
                        <!-- <input type="number" class="cart-quantity" min="1" value="${dataItem.lenseQuantity}" data-actual=${dataItem.lenseQuantity} data-id=${dataItem.element._id} data-lense="${dataItem.lense}">
                        <label for="${dataItem.element._id + dataItem.lense}" class="cart-label icon-check"></label> -->
                    </span>
                </div>
            </li>
        `;
    })

    return itemCartTemplate;
}

async function displayTemplateCart() {

    let template = await templateCart();
    document.getElementsByClassName('cart')[0].innerHTML += template;
}

/* --- INITIALISATION DE LA PAGE --- */
function initConfirmPage() {

    var cart = getLocalStorage();

    if (cart.itemsQuantity > 0) {

        displayTemplateCart()
            .then(function () {

                //Affichage des infos de la commande
                displayOrderInfos()
                    .catch(
                        confirmErrorAPI('cmd-form')
                    );
            })
            .then(function () {
                localStorage.clear();
                return true;
            })
            .catch(function () {
                confirmErrorAPI('main-cart')
            });
    } else {
        //Fallback si aucun item dans le panier
        confirmErrorNoItem();
    }
}

document.addEventListener("DOMContentLoaded", initConfirmPage)