/*let result = document.getElementsByClassName('result')[0];

fetch('http://localhost:3000/api/cameras/')
.then( (res) => res.json() )
.then( function(da){
    console.log( firstItem.author.login );
    da.forEach(element => {
      console.log("*********************************************************");
      console.log(element);
      template(element);
    });
    console.log(da)
});

function template(element){
  let tempA = `<div>
                  <span>${element.author.login}</span>
                  <span> - </span>
                  <span>${element.author.id}</span>
               </div>`;
  result.insertAdjacentHTML('beforeend', tempA);
}*/

fetch("http://localhost:3000/api/cameras/")
.then( /*(res) => res.json()*/ function(res){
  return res.json();
} )
.then( function(da){
    console.log("****** TOUS LES APPAREILS PHOTO ******");
    console.log(da)
});

fetch("http://localhost:3000/api/cameras/5be1ed3f1c9d44000030b061")
.then( (res) => res.json() )
.then( function(da){
    console.log("****** UN APPAREIL PHOTO ******");
    console.log(da)
});

let info = {
  contact : {
    firstName : "Prénom",
    lastName : "Nom",
    address : "UneFausseAdresse",
    city : "SpaceTown",
    email : "antoine.marteau@fake.fr",
  },
  products : ["5be1ed3f1c9d44000030b061","5be1ef211c9d44000030b062"]
}

//console.log(info);
console.log(JSON.stringify(info));

let options = {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json;charset=utf-8'
  },
  body: JSON.stringify(info)//,

};

fetch("http://localhost:3000/api/cameras/order", options)
.then( function(res){
  //console.log("****** COMMANDE APPAREIL PHOTO ******");
  console.log("****** Étape 1 ******");
  console.log(res);
  let responseJSON = res.json();
  console.log( responseJSON );
  return responseJSON;
} )
.then( function(da){
    console.log("****** Étape 2 ******");
    console.log(da);
});

/* TEST DU STORAGE */

localStorage.test = "Essai du localStorage";
sessionStorage.test = "Essai du localStorage";

console.log("localStorage :", localStorage);
console.log("sessionStorage :", sessionStorage);